#+TITLE: Hosting provided by ungleich.ch

[[https://ungleich.ch/][ungleich.ch]] is supporting openEngiadina with a virtual machine for hosting
website, instance of backend and applications.

* VM details

VM ID : VM25645
IP : 2a0a:e5c0:2:12:0:f0ff:fea9:c478
Debian 10, 1 CPU, 2GB RAM, 10GB SSD
You can access as root: ~ssh root@2a0a:e5c0:2:12:0:f0ff:fea9:c478~

* VPN

VM does not have an IPv4. To access the VM (for administrative purposes) from an
IPv4-Network you can use the ungleich IPv6 VPN. See the [[./vpn][vpn]] folder for more information.

* TOR

SSH access is also possible via a onion service:

r3h4houcvdccjkac6iyqvqsffglinw332hsapd6mzwfelyo3256wfqid.onion
