#+TITLE: Ungleich VPN

Ungleich has provided us VPN access. This is very useful as it can also be used
to access the IPv6-only host from IPv4.

This folder contains wireguard private key for accessing the ungleich VPN.

Public key can be derived from private key:

#+BEGIN_SRC shell
wg pubkey < privatekey
#+END_SRC

#+RESULTS:
: jmfcpHKfdKabqykO9l7YLMVDZUWFZuS8givRbynD834=

To connect to the VPN (as root):

#+BEGIN_SRC shell
wg-quick up ./wg0.conf
#+END_SRC

And to disconnect:

#+BEGIN_SRC shell
wg-quick down ./wg0.conf
#+END_SRC
