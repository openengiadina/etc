# openEngiadina: Prototype Fund Bewerbung

## Projekttitel

openEngiadina: Offen, sicher und dezentral - ein soziales Netzwerk für kleine Gemeinden in der Schweiz

## Beschreibt euer Projekt in 2-3 Sätzen (max 250)  

Der Austausch über kulturelle, soziale und politische Aktivitäten ist für kleine Gemeinden zentral. openEngiadina entwickelt ein soziales Netzwerk für lokales Wissen, bei welchem der Betrieb und die Daten bei der Gemeinde bleiben.  

## Welche gesellschaftliche Herausforderung möchtet ihr mit eurem Projekt lösen? (max 600)  

Das Internet schafft neue Möglichkeiten für Partizipation. Kleine Gemeinden in der Schweiz profitieren davon aber kaum. Es fehlt an Ressourcen um eine eigene Plattform aufzubauen oder es muss, zu Kosten der Datensicherheit und Datensouveränität, auf kommerzielle Anbieter vertraut werden.  

openEngiadina entwickelt eine Plattform wo die Daten bleiben wo sie produziert und benötigt werden: Bei den Gemeinden und ihren EinwohnerInnen. openEngiadina soll von Gemeinden selber betrieben und entwickelt werden um einen nachhaltigen, sicheren und offenen Austausch über Lokales zu ermöglichen.  

## Wer in der Schweizer Bevölkerung soll eure digitale Lösung nutzen? (Zielgruppe) (max 600)    

- EinwohnerInnen: Austausch über lokale Angelegenheiten  
- lokale Vereine: Über kulturelle und soziale Veranstaltungen informieren  
- Amtsstellen: EinwohnerInnen informieren (z.Bsp Feuerwehr informiert über Brandgefahr)  
- Gemeindepolitik: Direkte und klare Kommunikation über die Handlungen der Gemeinde  
- lokales Gewerbe und Selbständige: EinwohnerInnen über Angebote informieren  

## Wie wird eure digitale Lösung die demokratische Mitwirkung stärken? (max 600)

Demokratie heisst, dass sich alle informieren und äussern können, bevor gemeinschaftliche Entscheide gefällt werden. Mit openEngiadina wollen wir BürgerInnen von kleinen Gemeinden eine Plattform bieten wo sie sich über lokale Aktivitäten informieren können und sich niederschwellig beteiligen können.

## Bitte versucht, euer Projekt in den Politikzyklus einzuordnen (siehe https://prototypefund.opendata.ch/about/digitale-partizipation-und-demokratie/). Ihr könnt 1-4 Kategorien auswählen.    

- Informationen bereitstellen  
- Fachwissen einbringen  
- Ideen vorschlagen  
- Diskutieren  

## Wie baut ihr auf bestehenden analogen oder digitalen Lösungen in der Schweiz und im Ausland auf? (max 600)  

openEngiadina baut auf dem offenen, föderalisiertem Protokoll ActivityPub auf. ActivityPub wird bereits für dezentrale soziale Netzwerke genutzt (etwa 1.5 Millionen Benutzer auf 5000 Server). Die Plattform kann existierende Software nutzen und lässt sich mit dem existierenden Netzwerk verbinden.  

Die Verwendung von Linked Data als grundlegendes Datenmodel erlaubt die Einbindung von vielen bereits existierenden offenen Datenquellen (z.Bsp. swisstopo, swissbib, histhub.ch oder Wikipedia).  

## Welche Erfahrungen bereiten euch auf dieses Projekt vor? Wenn ihr jemals an Open-Source-Projekten gearbeitet haben, gibt bitte Links zu max. 3 Projekten an. (max 600)    

Pukkamustard ist Entwickler und "Freie Software"-Aktivist. Aktuelle Projekte beinhalten:  

- openEngiadina (https://gitlab.com/openengiadina)  
- Computer Lib, eine Serie von Kursen um Computer besser zu verstehen (https://inqlab.net/computer-lib.html)  
- Mitarbeit an vielen Open-Source-Projekten (NixOS, Guix, OCaml). Siehe <https://github.com/pukkamustard>  

## Wenn wir eine Person finden, die über die erforderlichen Fähigkeiten verfügt, wärt ihr offen für eine Zusammenarbeit mit dieser Person? (Ihr habt natürlich immer noch die Möglichkeit, Nein zu sagen).  

Ja.  

## Habt ihr schon an eurer Idee gearbeitet?  

Das soziale Netzwerk für Gemeinden wird auf dem openEngiadina Projekt aufbauen.  

Das Projekt openEngiadina wurde 2019 im Engadin gestartet, um touristische Daten direkt und einfach zu publizieren. In diesem Rahmen wurden erste Prototypen gebaut und getestet. Seit Februar 2020 wird openEngiadina von der NLNet Foundation gefördert. Die Förderung erlaubte eine Ausdehnung des Anwendungszwecks und die Entwicklung der Basiskomponente.  

Erste Versionen des Backends und der komplementären Webanwendung wurde bereits veröffentlicht.  

## Bitte skizziert die 4 wichtigsten Meilensteine, die du und dein Team während der Förderperiode umsetzen möchtet. Ihr könnt euch auch auf ergänzende Dokumente beziehen, die später hochgeladen werden können. (max 500)  

1. Erster Prototyp: Erlaubt einfache Kommunikation (Kurzinformationen und Kommentare). Grundlage für erstes Feedback.
3. Strukturierte Inhalte: Erstellen und interagieren mit strukturierten Inhalten (z.Bsp. Amtsmeldungen, Anlässe, Vereinsbeschreibungen)
5. Gruppen und Projekte: Organisieren von Inhalten in Gruppen und Projekten.
4. Task-Verwaltung: Status über laufende Projekte und Pendenzen transparent kommunizieren und Partizipation fördern (z.Bsp. Meldung von Infrastrukturschäden)


## Habt ihr bereits darüber nachgedacht, wie ihr oder andere euer Projekt nach dem Ende der Förderperiode weiter verbessern könnten? (Nachhaltigkeit des Projekts) (max 600)  

Folgende Massnahmen würden zu einer langfristigen Nutzung des Projekts beitragen:  

- Weitere Gemeinden sowie Vereine und lokales Gewerbe als Nutzer gewinnen  
- Gemeinnütziger Verein für den Betrieb und die Weiterentwicklung der Plattform (z.Bsp. mit existierenden Organisationen wie dem Verband Schwyzer Gemeinden und Bezirke)  
- Kommerzieller Support für Betreibung von Instanzen (in Zusammenarbeit mit existierenden Hostinganbietern wie ungleich.ch)  
- Schulungskonzepte für Nutzer (Amtsstellen und EinwohnerInnen)  
- Mitarbeit an der Platform mit Projektarbeiten an Schulen oder Hackathons fördern  

## Habt ihr eure zukünftigen Nutzer (eure Zielgruppe) bereits identifiziert oder sogar mit ihnen gesprochen? (max 600)  

Das Projekt wird in enger Zusammenarbeit mit der Gemeinde Feusisberg im Kanton Schwyz durchgeführt.  

Die Gemeinde möchte eine digitalen Plattform für die Kommunikation mit und unter den BürgerInnen. Mit der Plattform muss aber die Sicherheit, der Datenschutz und die Datensouveränität gewährleistet werden.  

Die Platform wird nach den Anforderungen der Gemeinde entwickelt und vor Ort getestet. Die Gemeinde prüft nach Projektende die weitere Nutzung der Plattform.  

## Was sind eure Überlegungen zum Datenschutz und zur Datensicherheit? Welche anderen Risiken habt ihr berücksichtigt? (siehe FAQ) (max 600)    

Bei openEngiadina sind die Gemeinden selbst Betreiber. Dabei bleibt die Datensouveränität lokal, aber damit auch die Verantwortung dem Datenschutz gerecht zu werden.

Den BenutzerInnen soll klar kommuniziert werden, wie und wo welche Daten verwendet werden. Für die Konzeption einer datenschutzgerechter Benutzeroberflächen zählen wir auf die Hilfe von SimplySecure.

Weitere Herausforderung ist die Moderation von Inhalten: Wie gehen Gemeinden (als Betreiber) mit unerwünschten Inhalten um? Dabei wollen wir etablierte Moderationstools von existierenden Plattformen (z.Bsp. Pleroma) übernehmen.

## Hier könnt ihr uns alle weiteren Informationen mitteilen, die wir kennen sollten. Ihr könnt euch auch auf ergänzende Dokumentation beziehen und diese dann im Abschnitt Anhänge hochladen. (max 600)  

openEngiadina betreibt die Webseite <https://openengiadina.net/>, wo Informationen über den Projektverlauf publiziert werden.
